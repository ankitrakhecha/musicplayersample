package com.example.gmc.musicplayersample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static final int ITEM_HEIGHT = 100;
    private  LinearLayout l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getActionBar().show();
       l= findViewById(R.id.linear_main_layout);
        ViewTreeObserver observer = l.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                init(l);
                l.getViewTreeObserver().removeGlobalOnLayoutListener(
                        this);
            }
        });

    }
    protected void init(LinearLayout l) {
        float a= Utils.convertPixelsToDp(l.getHeight(),this);

        for(int i=1;i<=Utils.convertPixelsToDp(a,this)/ITEM_HEIGHT;i++){
            LayoutInflater inflater = LayoutInflater.from(this);
            View inflatedLayout= inflater.inflate(R.layout.layout_list_item, null, false);
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams (
                    LinearLayout.LayoutParams .MATCH_PARENT, LinearLayout.LayoutParams .WRAP_CONTENT);
            inflatedLayout.setLayoutParams(lparams);
            l.addView(inflatedLayout);
        }

    }
}
